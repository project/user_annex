<?php

/**
 * @file
 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
 */

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\user_annex\Entity\UserAnnex;
use Drupal\Core\Url;
use Drupal\Core\Datetime\DrupalDateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Function to add one or more fields to the user_annex entity to allow
 * tracking of changes to the same field on the user entity.  This function
 * will be executed in a batch context.
 *
 * @param array $tracked_fields
 * @param array $context
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function user_annex_add_tracked_fields(array $tracked_fields, array &$context): void {

  // Get the field definitions for fields attached to user entity.
  $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('user', 'user');

  // Get the field config from user fields for each field to be added,
  // and add fields to user_annex entity.
  foreach($tracked_fields as $field_name) {

    /** @var \Drupal\field\Entity\FieldConfig $user_field */
    $user_field = $field_definitions[$field_name];

    $field_storage = FieldStorageConfig::loadByName('user_annex', $field_name);
    if (empty($field_storage)) {
      // Get field storage values from user field and adjust for user_annex.
      $field_storage_values = $user_field->getFieldStorageDefinition()->toArray();
      if ($user_field instanceof BaseFieldDefinition) {
        $field_storage_values['type'] = $user_field->getType();
        //$field_storage_values['field_name'] = 'base_' . $field_storage_values['field_name'];
      }
      else {
        unset($field_storage_values['uuid']);
        unset($field_storage_values['id']);
      }
      $field_storage_values['entity_type'] = 'user_annex';
      $field_storage_values['module'] = 'user_annex';
      $field_storage_values['dependencies'] = [
        'module' => ['user_annex'],
      ];
      // Create the field storage config entity.
      $field_storage = FieldStorageConfig::create($field_storage_values);
      $field_storage->save();
    }

    $field_config = FieldConfig::loadByName('user_annex', 'user_annex', $field_name);
    if (empty($field_config)) {
      // Get field instance values from user field and adjust for user_annex.
      $field_config_values = $user_field->toArray();
      if ($user_field instanceof BaseFieldDefinition) {
        $field_config_values['type'] = $user_field->getType();
        //$field_config_values['field_name'] = 'base_' . $field_config_values['field_name'];
      }
      else {
        unset($field_config_values['uuid']);
        unset($field_config_values['id']);
      }

      $field_config_values['field_storage'] = $field_storage;
      $field_config_values['entity_type'] = 'user_annex';
      $field_config_values['bundle'] = 'user_annex';
      $field_config_values['dependencies'] = [
        'config' => ['field.storage.' . $field_storage->id()],
        'module' => ['user_annex'],
      ];
      // Create the field instance config entity.
      $field_config = FieldConfig::create($field_config_values);
      $field_config->save();

      // Assign widget settings for the default form mode.
      /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
      $display_repository = \Drupal::service('entity_display.repository');
      $display_repository->getFormDisplay('user_annex', 'user_annex')
        ->setComponent($field_name, [
        ])->save();

      // Assign display settings for the 'default' and 'tab' view modes.
      $display_repository->getViewDisplay('user_annex', 'user_annex')
        ->setComponent($field_name, [
          'label' => 'inline',
        ])->save();
      $display_repository->getViewDisplay('user_annex', 'user_annex', 'tab')
        ->removeComponent($field_name)->save();
    }

    // Log message about new field added.
    \Drupal::logger('user_annex')->info('Field %label (%name) added to user_annex entity.',
      ['%label' => $field_config->getLabel(), '%name' => $field_name]);

  }

  // Update the details of the running config
  $config = \Drupal::config('user_annex.settings');
  \Drupal::state()->set('user_annex.base_fields', $config->get('base_fields'));
  \Drupal::state()->set('user_annex.attached_fields', $config->get('attached_fields'));

}

/**
 * Function to remove a field from the user_annex entity which is no longer
 * required to be tracked.  This function will be executed in a batch context.
 *
 * @param array $remove_fields
 * @param array $context
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function user_annex_remove_tracked_fields(array $remove_fields, array &$context): void {
  Drupal::messenger()->addMessage(t('Remove fields from user_annex: %fields', ['%fields'=>implode(', ', $remove_fields)]));

  // Delete the field instance config and field storage config for each field to be removed.
  foreach($remove_fields as $field_name) {

    $field_config = FieldConfig::loadByName('user_annex', 'user_annex', $field_name);
    if (!empty($field_config)) {
      $field_config->delete();
    }

    $field_storage = FieldStorageConfig::loadByName('user_annex', $field_name);
    if (!empty($field_storage)) {
      $field_storage->delete();
    }

    // Log message about existing field removed.
    \Drupal::logger('user_annex')->info('Field %label (%name) removed from user_annex entity.',
      ['%label' => $field_config->getLabel(), '%name' => $field_name]);

  }

  // Update the details of the running config
  $config = \Drupal::config('user_annex.settings');
  \Drupal::state()->set('user_annex.base_fields', $config->get('base_fields'));
  \Drupal::state()->set('user_annex.attached_fields', $config->get('attached_fields'));

}

/**
 * Function to create initial user_annex records for all users currently in
 * the database.  This function will be executed in a batch context.
 *
 * @param integer $batch_size
 * @param array $context
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function user_annex_create_initial_annex(int $batch_size, array &$context): void {

  $entity_type = 'user_annex';
  $values = [];
  $current_user = \Drupal::currentUser();

  $config = \Drupal::config('user_annex.settings');
  // Get a list of fields that should be tracked by the user_annex entity.
  $attached_fields = $config->get('attached_fields');
  $tracked_fields = [];
  foreach ($attached_fields as $field_name => $tracked) {
    if ($tracked) {
      $tracked_fields[] = $field_name;
    }
  }

  // Initialise the sandbox values on the first call.
  if (empty($context['sandbox'])) {
    $context['sandbox'] = [];
    $context['sandbox']['total'] = \Drupal::entityQuery('user')
      ->accessCheck(TRUE)
      ->condition('uid', 0, '>')
      ->count()
      ->execute();
    $context['sandbox']['last'] = 0;
    $context['sandbox']['count'] = 0;
    // Get current time for entitiy labels
    $now = new DrupalDateTime('now', date_default_timezone_get());
    $context['sandbox']['date'] = $now->format('Ymd-Hi');
    // Set the install date.
    \Drupal::state()->set('user_annex.install_date', $now->format('Y-m-d H:i'));
  }

  // Retrieve the next set of user accounts
  $uids = \Drupal::entityQuery('user')
    ->accessCheck(TRUE)
    ->condition('uid', $context['sandbox']['last'], '>')
    ->range(0, $batch_size)
    ->sort('uid')
    ->execute();

  $accounts = \Drupal::entityTypeManager()->getStorage('user')->loadMultiple($uids);

  /** @var \Drupal\user\Entity\User $account */
  foreach ($accounts as $account) {
    // Ignore the anonymous account.
    if (!$account->isAnonymous()) {

      $user_annex = new UserAnnex($values, $entity_type);
      // Generate a unique UUID value for this record.
      $user_annex->uuid->value = \Drupal::service('uuid')->generate();

      // Set the label for this user_annex entity
      $user_annex->setLabel($account->id() . ': Install - ' . $context['sandbox']['date']);

      // ?Set the account id for this annex record.
      $user_annex->setUserId($account->id());
      // Set the modified time to the last changed time of the account
      $user_annex->setCreatedTime($account->getChangedTime());
      // Set the modified_by uid value to current user.
      $user_annex->setModifiedByUid($current_user->id());
      // Record that this record was generated during module install.
      $user_annex->setAction('Install');

      // Get a list of fields attached to the user_annex entity.
      $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('user_annex', 'user_annex');
      // Add values of tracked fields attached to the user entity.
      foreach($tracked_fields as $field_name) {

        /** \Drupal\field\Entity\FieldConfig $field_definition */
        $field_definition = $field_definitions[$field_name];
        $field_type = $field_definition->getType();

        /** @var \Drupal\Core\Field\FieldItemList $field_item_list */
        $field_item_list = $account->get($field_name);

        $annex_values = user_annex_get_tracked_field_value($field_item_list);
        $user_annex->set($field_name, $annex_values);
      }
      // Mark this user_annex as a new entity.
      $user_annex->enforceIsNew(TRUE);

      $user_annex->save();

    }

    $context['sandbox']['last'] = $account->id();
    $context['sandbox']['count']++;
  }

  if ($context['sandbox']['count'] < $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
    if ($context['finished'] > 0.99) {
      $context['finished'] = 0.99;
    }
    $context['message'] = t('Processed %count of %total user accounts.', [
      '%count' => $context['sandbox']['count'],
      '%total' => $context['sandbox']['total'],
    ]);
  }
  else {
    $context['finished'] = 1;
    $context['message'] = t('Finished processing %total user accounts.', [
      '%total' => $context['sandbox']['total'],
    ]);
    $context['results']['total'] = $context['sandbox']['total'];
  }

}

/**
 * Completion function for batch operation to initialise the user_annex
 * records.
 *
 * @param boolean $success
 * @param array $results
 * @param array $operations
 *
 * @return \Symfony\Component\HttpFoundation\RedirectResponse
 */
function user_annex_finished_initial_annex(bool $success, array $results, array $operations): RedirectResponse {

  $messenger = \Drupal::messenger();

  if ($success) {
    // Flag the initialisation as complete.
    \Drupal::state()->set('user_annex.initialise_required',FALSE);
    // Display a completion message to the user.
    \Drupal::messenger()->addMessage(t('Added %total new user_annex records', ['%total' => $results['total']]));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $messenger
      ->addMessage(t('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]));
  }

  // Redirect the user to the system status report.
  return new RedirectResponse(Url::fromRoute('system.status')->setAbsolute()->toString());

}

/**
 * Function to update user_annex records when a tracked field is added or
 * removed from the annex processing.  This function is executed in a batch
 * context.
 *
 * @param integer $batch_size
 * @param array $add_fields
 * @param array $remove_fields
 * @param array $context
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function user_annex_update_tracked_annex(int $batch_size, array $add_fields, array $remove_fields, array &$context): void {

  $entity_type = 'user_annex';
  $values = [];
  $current_user = \Drupal::currentUser();

  $config = \Drupal::config('user_annex.settings');
  // Get a list of fields that should be tracked by the user_annex entity.
  $attached_fields = $config->get('attached_fields');
  $tracked_fields = [];
  foreach ($attached_fields as $field_name => $tracked) {
    if ($tracked) {
      $tracked_fields[] = $field_name;
    }
  }

  // Initialise the sandbox values on the first call.
  if (empty($context['sandbox'])) {
    $context['sandbox'] = [];
    $context['sandbox']['total'] = \Drupal::entityQuery('user')->accessCheck(TRUE)->condition('uid', 0, '>')->count()->execute();
    $context['sandbox']['last'] = 0;
    $context['sandbox']['count'] = 0;
    $context['sandbox']['updated'] = 0;
    // Get current time for entitiy labels
    $now = new DrupalDateTime('now', date_default_timezone_get());
    $context['sandbox']['date'] = $now->format('Ymd-Hi');
  }

  // Retrieve the next set of user accounts
  $uids = \Drupal::entityQuery('user')
    ->accessCheck(TRUE)
    ->condition('uid', $context['sandbox']['last'], '>')
    ->range(0, $batch_size)
    ->sort('uid')
    ->execute();

  $accounts = \Drupal::entityTypeManager()->getStorage('user')->loadMultiple($uids);

  /** @var \Drupal\user\Entity\User $account */
  foreach ($accounts as $account) {
    // Ignore the anonymous account.
    if (!$account->isAnonymous()) {

      // Set flag to initially suppress writing a user_annex record to database.
      $user_annex_update = FALSE;

      // Create a new user_annex entity.
      $user_annex = new UserAnnex($values, $entity_type);
      // Generate a unique UUID value for this record.
      $user_annex->uuid->value = \Drupal::service('uuid')->generate();

      // Empty arrays for list of new fields added or old fields removed from
      // the user_annex record for this account.
      $new_fields = [];
      $old_fields = [];

      // Get a list of fields attached to the user_annex entity.
      $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('user_annex', 'user_annex');
      // Process each of these defined fields.
      foreach ($field_definitions as $field_name => $field_definition) {
        // Ignore base field definitions and process only attached fields
        /** \Drupal\field\Entity\FieldConfig $field_definition */
        if ($field_definition instanceof FieldConfig) {
          /** @var \Drupal\Core\Field\FieldItemList $field_item_list */
          $field_item_list = $account->get($field_name);

          // Record the value (if any) of the attached field in the user_annex record?
          if (!empty($field_item_list)) {
            $field_type = $field_definition->getType();

            // Check if account has a value for the field being removed from tracking.
            if (in_array($field_name, $remove_fields) && $field_item_list->count() > 0) {
              // Yes, so set a flag to save a user_annex record and record this as a removed field.
              $user_annex_update = TRUE;
              $old_fields[] = $field_definition->getLabel();
            }
            elseif (in_array($field_name, $add_fields) && $field_item_list->count() > 0) {
              // Account has a value for a field which is being added to tracking,
              // so extract the values and add to a user_annex record.
              $annex_values = user_annex_get_tracked_field_value($field_item_list);
              if (!empty($annex_values)) {
                // Set a flag to create a new user_annex record this as a new attached field
                $user_annex_update = TRUE;
                $user_annex->set($field_name, $annex_values);
                $new_fields[] = $field_definition->getLabel();
              }
            }
            elseif (in_array($field_name, $tracked_fields) && $field_item_list->count() > 0) {
              // Account has a value for a field which was already being tracked,
              // so extract the values and add to a user_annex record.
              $annex_values = user_annex_get_tracked_field_value($field_item_list);
              if (!empty($annex_values)) {
                // Do not set a flag to create a new user_annex record,
                // or record the field name as being added or removed.
                $user_annex->set($field_name, $annex_values);
              }
            }
          }
        }
      }

      $difference = [];
      if (!empty($new_fields)) {
        $difference[] = 'Add field values: ' . implode(', ', $new_fields);
      }
      if (!empty($old_fields)) {
        $difference[] = 'Remove field values: ' . implode(', ', $old_fields);
      }

      // If the user_annex entry contains values for any attached fields,
      // then populate the base fields and save.
      if ($user_annex_update) {
        // Set the label for this user_annex entity
        $user_annex->setLabel($account->id() . ': Modify - ' . $context['sandbox']['date']);

        // Set the modified time to the last changed time of the account
        $user_annex->setCreatedTime($account->getChangedTime());
        // Set the modified_by uid value to current user.
        $user_annex->setModifiedByUid($current_user->id());
        // Record that this record was generated during tracked field update.
        $user_annex->setAction('Modify tracked fields');

        $user_annex->setUserId($account->id());

        // Record the reason for adding this user_annex record.
        $user_annex->setDifference(implode('. ', $difference));

        // Mark this user_annex as a new entity.
        $user_annex->enforceIsNew(TRUE);
        $user_annex->save();
        $context['sandbox']['updated']++;
      }

    }

    $context['sandbox']['last'] = $account->id();
    $context['sandbox']['count']++;
  }

  if ($context['sandbox']['count'] < $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
    if ($context['finished'] > 0.99) {
      $context['finished'] = 0.99;
    }
    $context['message'] = t('Processed %count of %total user accounts.', [
      '%count' => $context['sandbox']['count'],
      '%total' => $context['sandbox']['total'],
    ]);
  }
  else {
    $context['finished'] = 1;
    $context['message'] = t('Finished processing %total user accounts. Added %num new user annex records', [
      '%total' => $context['sandbox']['total'],
      '%num' => $context['sandbox']['updated'],
    ]);
    $context['results']['total'] = $context['sandbox']['total'];
    $context['results']['updated'] = $context['sandbox']['updated'];
  }

}

/**
 * Completion function for batch operation to update the user_annex
 * records.
 *
 * @param boolean $success
 * @param array $results
 * @param array $operations
 *
 * @return \Symfony\Component\HttpFoundation\RedirectResponse
 */
function user_annex_finished_update_attached_fields(bool $success, array $results, array $operations): RedirectResponse {

  $messenger = Drupal::messenger();

  if ($success) {
    // Flag the update as complete.
    \Drupal::state()->set('user_annex.base_fields_update_required',FALSE);
    \Drupal::state()->set('user_annex.attached_fields_update_required',FALSE);
    // Display a completion message to the user.
    \Drupal::messenger()->addMessage(t('Added %num new user_annex records', ['%num' => $results['updated']]));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $messenger
      ->addMessage(t('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]));
  }

  // Redirect to the system status report page.
  return new RedirectResponse(Url::fromRoute('system.status')->setAbsolute()->toString());

}
