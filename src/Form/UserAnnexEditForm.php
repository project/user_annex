<?php

namespace Drupal\user_annex\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form to edit user_annex entities.
 *
 * @ingroup user_annex
 */
class UserAnnexEditForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    /* @var \Drupal\user_annex\Entity\UserAnnex $entity */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $status = parent::save($form, $form_state);
    $entity = $this->entity;

    $this->messenger()
      ->addMessage($this->t('The user_annex %feed has been updated.', ['%feed' => $entity->toLink()->toString()]));

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $status;
  }

}
