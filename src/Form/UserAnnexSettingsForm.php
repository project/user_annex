<?php

namespace Drupal\user_annex\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UserAnnexSettingsForm.
 *
 * @ingroup user_annex
 */
class UserAnnexSettingsForm extends ConfigFormBase {

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new UserAnnexSettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($config_factory);
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container): ConfigFormBase|UserAnnexSettingsForm|static {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * Config settings.
   *
   * @var string
   */
  const MY_SETTINGS = 'user_annex.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::MY_SETTINGS,
    ];
  }
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId(): string {
    return 'user_annex_settings';
  }

  /**
   * Defines the settings form for user_annex entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config(static::MY_SETTINGS);

    $message = ['Settings form for user_annex entities.'];
    $message[] = 'Select fields attached to the user entity that should be tracked in the annex records.';
    $message[] = 'Note that currently there is no provision to change which user base fields are tracked.';
    $message[] = 'The default is to track all user base fields and none of the attached fields.';


    $form['user_annex_settings']['#markup'] = implode('<br />', $message);

    // Specify the user properties that make up the user_annex base fields.
    $user_properties = user_annex_get_base_properties();

    // Return the values as a tree
    $form['#tree'] = TRUE;

    // Specify a fieldset for records with no differences.
    $form['no_change'] = [
      '#type' => 'details',
      '#title' => 'No change',
      '#open' => FALSE,
    ];

    $form['no_change']['ignore'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ignore user save when there is no change to tracked fields.'),
      '#default_value' => $config->get('no_change.ignore'),
    ];

    $description = 'Delete "no change" records older than the specified date. <br />';
    $description .= 'Leave blank to suppress deletion, or enter a valid php date period such as "5 days", "3 months", etc.<br />';
    $description .= 'Has no effect if "no change" records are ignored, once all such records are deleted.<br />';

    $form['no_change']['delete'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delete'),
      '#size' => 16,
      '#default_value' => $config->get('no_change.delete'),
      '#description' => $this->t($description),
    ];

    $form['no_change']['del_instructions'] = [
      '#type' => 'markup',
      '#markup' => $this->t('')
    ];

    $form['no_change']['batch'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Batch size'),
      '#size' => 16,
      '#default_value' => $config->get('no_change.batch') ?: 100,
      '#description' => $this->t('Maximum number of "no change" user annex records to delete in a cron run. Specify an integer value (zero for no limit).'),
    ];

    // Specify a fieldset for base fields.
    $form['base_fields'] = [
      '#type' => 'details',
      '#title' => 'Base fields',
      '#open' => FALSE,
    ];
    // Specify a fieldset for attached fields.
    $form['attached_fields'] = [
      '#type' => 'details',
      '#title' => 'Attached fields',
      '#open' => TRUE,
    ];

    $field_definitions = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    $weight = 0;
    foreach ($field_definitions as $field_name => $field_definition) {
      $weight++;
      if ($field_definition instanceof BaseFieldDefinition) {
        if (in_array($field_name, $user_properties)) {
          // Define an entry for this field.
          $form['base_fields'][$field_name] = [
            '#type' => 'checkbox',
            '#title' => $field_definition->getLabel(),
            '#description' => $field_definition->getDescription(),
            '#default_value' => $config->get('base_fields.' . $field_name),
            '#weight' => $weight,
          ];
        }
      }
      else {
        // Field added through UI or programatically.
        /** @var \Drupal\field\Entity\FieldConfig $field_config */
        $field_config = $field_definition;
        $form['attached_fields'][$field_name] = [
          '#type' => 'checkbox',
          '#title' => $field_config->getLabel(),
          '#description' => $field_config->getDescription(),
          '#default_value' => $config->get('attached_fields.' . $field_name),
          '#weight' => $weight,
        ];
      }
    }

    // Specify a fieldset for the archive fields
    $form['archive'] = [
      '#type' => 'details',
      '#title' => 'Archive settings',
      '#open' => FALSE,
    ];

    $form['archive']['directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Directory'),
      '#description' => $this->t('Directory to save archive files.  It will be in the "private://" file system unless this is not defined.'),
      '#default_value' => $config->get('archive.directory') ?: 'user-annex/archive/',
    ];

    $form['archive']['filename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File name'),
      '#description' => $this->t('Specify the filename for archive files.  The date/time of the archive will be appended to this name.'),
      '#default_value' => $config->get('archive.filename') ?: 'user-annex-archive',
    ];

    $form['archive']['max_cardinality'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Max cardinality'),
      '#description' => $this->t('Specify the maximum cardinality for a field and its properties to have separate columns in a CSV file.<br/>
       Fields with a greater cardinality will be serialized in a single column.'),
      '#default_value' => $config->get('archive.max_cardinality') ?: 3,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    // Check that the "no change" delete period is a valid period in a form
    // used by php date, ie 1 Year, 8 Months etc
    if ($delete_period =  trim($values['no_change']['delete'])) {
      try {
        $delete_date = new \DateTime('now -' . $delete_period);
      } catch (\Exception $e) {
        $form_state->setErrorByName('no_change][delete', $this->t('Delete period must be a valid date string like "5 days", "3 months", etc'));
      }
    }
    // Check that the batch size is a valid integer.
    if ($batch_size = $values['no_change']['batch']) {
      if (!is_numeric($batch_size) || (int)$batch_size != $batch_size || $batch_size < 0) {
        $form_state->setErrorByName('no_change][batch', $this->t('Batch size must be an integer greater than zero.'));
      }
    }

    // Check that the max_cardinality is both numeric and reasonable.
    $max_cardinality = trim($values['archive']['max_cardinality']);
    if (!is_numeric($max_cardinality) || $max_cardinality != (int)$max_cardinality || $max_cardinality < 1 || $max_cardinality > 20) {
      $form_state->setErrorByName('archive][max_cardinality', $this->t('Max cardinality must be an integer between 1 and 20!'));
    }

  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Retrieve the configuration.
    $config = $this->configFactory->getEditable(static::MY_SETTINGS);

    $values = $form_state->getValues();

    $config->set('no_change.ignore', $values['no_change']['ignore']);
    $config->set('no_change.delete', trim($values['no_change']['delete']));

    foreach ($values['base_fields'] as $field_name => $value) {
      $config->set('base_fields.' . $field_name, $value);
    }

    foreach ($values['attached_fields'] as $field_name => $value) {
      $config->set('attached_fields.' . $field_name, $value);
    }

    // Trim any leading/trailing space or directory separator chars and add trailing one.
    $config->set('archive.directory', trim($values['archive']['directory'], ' /') . '/');
    $config->set('archive.filename', trim($values['archive']['filename']));
    $config->set('archive.max_cardinality', trim($values['archive']['max_cardinality']));

    $config->save();

    // Check whether the initialise process has run.
    $initialised = ! \Drupal::state()->get('user_annex.initialise_required');

    // Get the current running config and compare with the updated config.
    $current_base_fields = \Drupal::state()->get('user_annex.base_fields');
    if ($initialised && $values['base_fields'] != $current_base_fields) {
      // Set a flag to remind that the batch field update needs to be run.
      \Drupal::state()->set('user_annex.base_fields_update_required', TRUE);
    }
    else {
      // Clear flag indicating that the batch field update needs to be run.
      \Drupal::state()->set('user_annex.base_fields_update_required', FALSE);
    }

    $current_attached_fields = \Drupal::state()->get('user_annex.attached_fields');
    if ($initialised && $values['attached_fields'] != $current_attached_fields) {
      // Set a flag to remind that the batch field update needs to be run.
      \Drupal::state()->set('user_annex.attached_fields_update_required', TRUE);
    }
    else {
      // Clear flag indicating that the batch field update needs to be run.
      \Drupal::state()->set('user_annex.attached_fields_update_required', FALSE);
    }

    parent::submitForm($form, $form_state);

  }

}
