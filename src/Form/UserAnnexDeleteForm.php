<?php

namespace Drupal\user_annex\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Provides a form to delete user_annex entities.
 *
 * @ingroup user_annex
 */
class UserAnnexDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId(): string {
    return 'user_annex_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to delete entity %name?', array('%name' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   *
   * If the delete command is canceled, return to the contact list.
   */
  public function getCancelUrl(): Url {
    return new Url('entity.user_annex.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * Delete the entity and log the event. logger() replaces the watchdog.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->delete();

    $this->logger('user_annex')->notice('deleted %title.', ['%title' => $this->entity->label()]);
    // Redirect to term list after delete.
    $form_state->setRedirect('entity.user_annex.collection');
  }
}
