<?php

namespace Drupal\user_annex\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements the Batch Install Form.
 */
class UserAnnexInitialiseForm extends FormBase {

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new UserAnnexSettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container): UserAnnexInitialiseForm|static {
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId(): string {
    return 'user_annex_batch_install';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state): RedirectResponse|array {

    // If the user annex records have already been initialised, then re-direct
    // the user to the system status report.
    if (!\Drupal::state()->get('user_annex.initialise_required')) {
      \Drupal::messenger()->addMessage(t('User annex records have already been initialised!'));
      return new RedirectResponse(Url::fromRoute('system.status')->setAbsolute()->toString());
    }

    $config = \Drupal::config('user_annex.settings');

    $message = 'This process will generate the initial values of the user annex records from the user entities in the database.';
    $message .= '<br />' . 'To change the fields to be tracked, CANCEL this initialise and go to the <a href=":settings">User annex settings</a> form.';
    $message .= '<br />' . 'There are currently %count user records to be processed.';
    $message_args = [
      '%count' => \Drupal::entityQuery('user')->accessCheck(TRUE)->condition('uid', 0, '>')->count()->execute(),
      ':settings' => Url::fromRoute('user_annex.settings')->toString(),
    ];

    $form['notice'] = [
      '#markup' => '<div>' . $this->t($message, $message_args) . '</div>',
    ];

    // Get a list of fields attached to the user entity.
    $field_definitions = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    $field_options = [];

    // Get a list of fields that should be tracked by the user_annex entity.
    $tracked_fields = [];
    $fields = array_merge($config->get('base_fields'), $config->get('attached_fields'));
    foreach ($fields as $field_name => $tracked) {
      if ($tracked) {
        $tracked_fields[] = $field_name;
      }
      /** @var \Drupal\field\Entity\FieldConfig $field_definition */
      $field_definition = $field_definitions[$field_name];
      $field_options[$field_name] = $field_definition->getLabel();
    }

    if (!empty($tracked_fields)) {
      $form['tracked_fields'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Add fields'),
        '#description' => $this->t('The above fields will be added to user annex tracking.'),
        '#options' => $field_options,
        '#default_value' => $tracked_fields,
        '#disabled' => TRUE,
      ];
    }

    $form['batch_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Batch size'),
      '#description' => $this->t('Enter the number of user accounts to process in each batch'),
      '#default_value' => 100,
      '#required' => TRUE,
    ];

    $form['submit_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start Batch'),
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $batch_size = (int) $form_state->getValue('batch_size');
    if ($batch_size < 1 || $batch_size > 1000) {
      $form_state->setErrorByName('batch_size', $this->t('Invalid batch size. Specify an integer between 1 and 1000.'));
    }
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $module_path = \Drupal::service('module_handler')->getModule('user_annex')->getPath();

    $batch = [
      'title' => t('Installing initial user annex records...'),
      'operations' => [],
      'init_message' => t('Commencing'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('An error occurred during processing'),
      'finished' => 'user_annex_finished_initial_annex',
      'file' => $module_path . '/user_annex.batch.inc',
    ];


    if (!empty($form_state->getValue('tracked_fields'))) {
      // Batch operation to define the new fields attached to the user_annex entity.
      $tracked_fields = array_filter($form_state->getValue('tracked_fields'));
      $batch['operations'][] = ['user_annex_add_tracked_fields', [$tracked_fields]];
    }

    // Batch operation to update the user annex records with changed field data.
    $batch_size = $form_state->getValue('batch_size');
    $batch['operations'][] = ['user_annex_create_initial_annex', [$batch_size]];

    batch_set($batch);

  }

}
