<?php

namespace Drupal\user_annex\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements the Batch Install Form.
 */
class UserAnnexArchiveForm extends FormBase {

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new UserAnnexSettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container): UserAnnexArchiveForm|static {
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId(): string {
    return 'user_annex_batch_archive';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state): RedirectResponse|array {

    // If the user annex records have not yet been initialised, then re-direct
    // the user to the initialise form instead.
    if (\Drupal::state()->get('user_annex.initialise_required')) {
      return new RedirectResponse(Url::fromRoute('user_annex.batch_install_form')->setAbsolute()->toString());
    }

    $config = \Drupal::config('user_annex.settings');

    $message = 'This process will archive specified user annex records by copying the details held in the database to an XML or CSV file';
    $message .= '<br />' . 'and then deleting the records from the database.  Note that if a user account has only a single annex record';
    $message .= '<br />' . 'in the database, then it will not be archived or deleted.';
    $message_args = [
    ];

    $form['notice'] = [
      '#markup' => '<div>' . $this->t($message, $message_args) . '</div>',
    ];

    // Select a date for archiving records.
    $default_date = $form_state->getValue('archive_date');
    if (empty($default_date)) {
      $default_date = ['year' => date('Y') - 1, 'month' => date('m'), 'day' => date('d')];
    }
    $form['archive_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Archive date'),
      '#description' => $this->t('Archive all user annex records older than or equal to this date.'),
      '#default_value' => $default_date,
    ];

    // Select a maximum number of annex records to retain for each account
    $form['min_records'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minumum records'),
      '#description' => $this->t('Specify the minimum number of annex records to be retained for each account'),
      '#default_value' => $form_state->getValue('min_records') ?? '1',
    ];

    // Select a maximum number of annex records to retain for each account
    $form['max_records'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum records'),
      '#description' => $this->t('Specify the maximum number of annex records to be retained for each account'),
      '#default_value' => $form_state->getValue('max_records') ?? '10',
    ];

    // Define the formats able to be used for the archive file.
    $file_formats = [
      'txt' => 'TXT - serialized string',
      'csv' => 'CSV - comma separated values',
    ];
    if (\Drupal::moduleHandler()->moduleExists('serialization')) {
      $file_formats['xml'] = 'XML - structured data';
      $file_formats['json'] = 'JSON - structured data';
    }

    $form['file_format'] = [
      '#type' => 'radios',
      '#title' => $this->t('File format'),
      '#description' => $this->t('Select the format for the archive file'),
      '#options' => $file_formats,
      '#default_value' => isset($file_formats['xml']) ? 'xml' : 'csv',
      '#required' => TRUE,
    ];

    $form['delete_records'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete records'),
      '#description' => $this->t('Select this box to have the records deleted from the database after successful archive.'),
      '#default_value' => FALSE,
    ];

    $form['batch_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Batch size'),
      '#description' => $this->t('Enter the number of user accounts to process in each batch'),
      '#default_value' => 100,
      '#required' => TRUE,
    ];

    $form['submit_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start Batch'),
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!empty($min_records = $form_state->getValue('min_records'))) {
      $min_records = (int) $min_records;
      if ($min_records < 1 || $min_records > 999) {
        $form_state->setErrorByName('max_records', $this->t('Invalid number of records to retain. Specify an integer between 1 and 999.'));
      }
    }

    if (!empty($max_records = $form_state->getValue('max_records'))) {
      $max_records = (int) $max_records;
      if ($max_records < 1 || $max_records > 9999) {
        $form_state->setErrorByName('max_records', $this->t('Invalid number of records to retain. Specify an integer between 1 and 9999.'));
      }
    }

    if (!empty($min_records) && !empty($max_records) && $min_records > $max_records) {
      $form_state->setErrorByName(('max_records'), $this->t('Max records must be equal to or larger than min records'));
    }

    $batch_size = (int) $form_state->getValue('batch_size');
    if ($batch_size < 1 || $batch_size > 1000) {
      $form_state->setErrorByName('batch_size', $this->t('Invalid batch size. Specify an integer between 1 and 1000.'));
    }

  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $archive_date = $form_state->getValue('archive_date');
    $min_records = $form_state->getValue('min_records');
    $max_records = $form_state->getValue('max_records');
    $file_format = $form_state->getValue('file_format');
    $batch_size = $form_state->getValue('batch_size');

    $module_path = \Drupal::service('module_handler')->getModule('user_annex')->getPath();

    $batch = [
      'title' => t('Archiving user annex records...'),
      'operations' => [],
      'init_message' => t('Commencing'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('An error occurred during processing'),
      'finished' => 'user_annex_finished_archive_records',
      'file' => $module_path . '/user_annex.archive.inc',
    ];

    // Operation to copy the user annex records to XML or CSV file.
    $batch['operations'][] = ['user_annex_archive_records', [$archive_date, $min_records, $max_records, $file_format, $batch_size]];

    if ($form_state->getValue('delete_records')) {
      // Operation to delete the archived user annex records.
      $batch['operations'][] = ['user_annex_delete_records', [$archive_date, $min_records, $max_records, $batch_size]];
    }

    batch_set($batch);

  }

}
