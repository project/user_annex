<?php

namespace Drupal\user_annex\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements the Batch Install Form.
 */
class UserAnnexUpdateForm extends FormBase {

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new UserAnnexSettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container): UserAnnexUpdateForm|static {
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId(): string {
    return 'user_annex_batch_update';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state): RedirectResponse|array {

    // If the user annex records have not yet been initialised, then re-direct
    // the user to the initialise form instead.
    if (\Drupal::state()->get('user_annex.initialise_required')) {
      return new RedirectResponse(Url::fromRoute('user_annex.batch_install_form')->setAbsolute()->toString());
    }

    $config = \Drupal::config('user_annex.settings');

    $message = 'This process will update the user annex records with values from fields attached to the user entities in the database.';
    $message .= '<br />' . 'To change the fields to be added or removed, CANCEL this update and go to the <a href=":settings">User annex settings</a> form.';
    $message .= '<br />' . 'There are currently %count user records to be processed.';
    $message_args = [
      '%count' => \Drupal::entityQuery('user')->accessCheck(TRUE)->condition('uid', 0, '>')->count()->execute(),
      ':settings' => Url::fromRoute('user_annex.settings')->toString(),
      ];

    $form['notice'] = [
      '#markup' => '<div>' . $this->t($message, $message_args) . '</div>',
    ];

    // Get a list of fields attached to the user entity.
    // Is an array of \Drupal\Core\Field\BaseFieldDefinition or \Drupal\field\Entity\FieldConfig
    $field_definitions = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    $field_options = [];

    // Get a list of fields that should be tracked by the user_annex entity.
    $tracked_fields = [];
    $fields = array_merge($config->get('base_fields'), $config->get('attached_fields'));
    foreach ($fields as $field_name => $tracked) {
      if ($tracked) {
        $tracked_fields[] = $field_name;
      }
      $field_definition = $field_definitions[$field_name];
      $field_options[$field_name] = $field_definition->getLabel();
    }

    // Get a list of fields currently attached to the user_annex entity.
    $current_fields = [];
    $field_definitions = $this->entityFieldManager->getFieldDefinitions('user_annex', 'user_annex');
    foreach ($field_definitions as $field_name => $field_definition) {
      if ($field_definition instanceof FieldConfig) {
        // Field added to user_annex either through UI or programatically.
        $current_fields[] = $field_name;
        $field_options[$field_name] .= ' (currently tracked)';
      }
    }

    $add_fields = array_diff($tracked_fields, $current_fields);
    $remove_fields = array_diff($current_fields, $tracked_fields);

    if (!empty($add_fields)) {
      $form['add_fields'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Add fields'),
        '#description' => $this->t('The above fields will be added to user annex tracking.'),
        '#options' => $field_options,
        '#default_value' => $add_fields,
        '#disabled' => TRUE,
      ];
    }

    if (!empty($remove_fields)) {
      $form['remove_fields'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Remove fields'),
        '#description' => $this->t('The above fields will be removed from user annex tracking.'),
        '#options' => $field_options,
        '#default_value' => $remove_fields,
        '#disabled' => TRUE,
      ];
    }


    $form['batch_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Batch size'),
      '#description' => $this->t('Enter the number of user accounts to process in each batch'),
      '#default_value' => 100,
      '#required' => TRUE,
    ];

    $form['submit_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start Batch'),
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $batch_size = (int) $form_state->getValue('batch_size');
    if ($batch_size < 1 || $batch_size > 1000) {
      $form_state->setErrorByName('batch_size', $this->t('Invalid batch size. Specify an integer between 1 and 1000.'));
    }
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $batch_size = $form_state->getValue('batch_size');
    $add_fields = $form_state->getValue('add_fields');
    $add_fields = is_array($add_fields) ? array_filter($add_fields) : [];
    $remove_fields = $form_state->getValue('remove_fields');
    $remove_fields = is_array($remove_fields) ? array_filter($remove_fields) : [];

    $module_path = \Drupal::service('module_handler')->getModule('user_annex')->getPath();

    $batch = [
      'title' => t('Updating attached fields for user annex records...'),
      'operations' => [],
      'init_message' => t('Commencing'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('An error occurred during processing'),
      'finished' => 'user_annex_finished_update_attached_fields',
      'file' => $module_path . '/user_annex.batch.inc',
    ];

    if (!empty($add_fields)) {
      // Batch operation to define the new fields attached to the user_annex entity.
      $batch['operations'][] = ['user_annex_add_tracked_fields', [$add_fields]];
    }

    // Batch operation to update the user annex records with changed field data.
    $batch['operations'][] = ['user_annex_update_tracked_annex', [$batch_size, $add_fields, $remove_fields]];

    if (!empty($remove_fields)) {
      // Batch operation to delete the old fields that were attached to user_annex entity.
      $batch['operations'][] = ['user_annex_remove_tracked_fields', [$remove_fields]];
    }

    batch_set($batch);

  }

}
