<?php

namespace Drupal\user_annex;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of user_annex entities.
 *
 * @ingroup user_annex
 */
class UserAnnexListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    //$header['id'] = $this->t('ID');
    $header['label'] = $this->t('Label');
    $header['user_name'] = $this->t('User name');
    $header['user_id'] = $this->t('User id');
    $header['modified_by'] = $this->t('Modified by');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   * @noinspection PhpPossiblePolymorphicInvocationInspection
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\user_annex\Entity\UserAnnex $entity */
    //$row['id'] = $entity->id();
    $row['label'] = Link::createFromRoute(
      $entity->label(),
      'entity.user_annex.canonical',
      ['user_annex' => $entity->id()]
    );
    $row['user_name'] = $entity->getUserName();
    $row['user_id'] = $entity->getUserId();
    /** @var \Drupal\user\Entity\User $modified_by */
    $modified_by = \Drupal::entityTypeManager()
      ->getStorage('user')
      ->load($entity->getModifiedBy());
    $row['modified_by'] = $modified_by->getDisplayName() . ' (' . $modified_by->id() . ')';
    return $row + parent::buildRow($entity);
  }

}
