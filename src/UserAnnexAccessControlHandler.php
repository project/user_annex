<?php

namespace Drupal\user_annex;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the user_annex entity.
 *
 * @see \Drupal\user_annex\Entity\UserAnnex.
 */
class UserAnnexAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    /** @var \Drupal\user_annex\Entity\UserAnnexInterface $entity */

    switch ($operation) {

      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view user_annex entities');

      case 'edit':
        return AccessResult::allowedIfHasPermission($account, 'edit user_annex entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete user_annex entities');
    }

    // Unknown operation, forbid.
    return AccessResult::forbidden('Unknown operation on user annex record!');
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResultInterface {
    return AccessResult::allowedIfHasPermission($account, 'add user_annex entities');
  }


}
