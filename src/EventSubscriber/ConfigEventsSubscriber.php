<?php

namespace Drupal\user_annex\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EntityTypeSubscriber.
 *
 * @package Drupal\user_annex\EventSubscriber
 */
class ConfigEventsSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
   */
  public static function getSubscribedEvents(): array {
    return [
      ConfigEvents::SAVE => 'configSave',
      ConfigEvents::DELETE => 'configDelete',
    ];
  }

  /**
   * React to a config object being saved.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Config crud event.
   */
  public function configSave(ConfigCrudEvent $event) {
    $config = $event->getConfig();
    if ($config->getName() == 'user_annex.settings') {
      // Check whether the initialise process has run.
      $initialised = ! \Drupal::state()->get('user_annex.initialise_required');

      // Get the current running config and compare with the updated config.
      if ($initialised) {
        $current_base_fields = \Drupal::state()->get('user_annex.base_fields');
        if ($config->get('base_fields') != $current_base_fields) {
          // Set a flag to remind that the batch field update needs to be run.
          \Drupal::state()->set('user_annex.base_fields_update_required', TRUE);
        }
        else {
          // Clear flag indicating that the batch field update needs to be run.
          \Drupal::state()->set('user_annex.base_fields_update_required', FALSE);
        }

        $current_attached_fields = \Drupal::state()->get('user_annex.attached_fields');
        if ($config->get('attached_fields') != $current_attached_fields) {
          // Set a flag to remind that the batch field update needs to be run.
          \Drupal::state()->set('user_annex.attached_fields_update_required', TRUE);
        }
        else {
          // Clear flag indicating that the batch field update needs to be run.
          \Drupal::state()->set('user_annex.attached_fields_update_required', FALSE);
        }
      }
      else {
        // Set a flag to remind that the batch initialisation needs to be run.
        \Drupal::state()->set('user_annex.initialise_required',TRUE);
      }
    }
  }

  /**
   * React to a config object being deleted.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Config crud event.
   */
  public function configDelete(ConfigCrudEvent $event) {
    $config = $event->getConfig();
    /** @noinspection PhpStatementHasEmptyBodyInspection */
    if ($config->getName() == 'user_annex.settings') {
      // The user_annex module is probably being uninstalled.  Actions are in
      // hook_unistall().
    }
  }

}
