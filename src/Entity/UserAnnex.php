<?php

namespace Drupal\user_annex\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;
use Drupal\user\Entity\User;

/**
 * Defines the user_annex entity.
 *
 * User annex entities are a content entity that is attached to a user record
 * to allow for the collection of data values from the user record over time
 * as the value in the user record changes.  An administration user with the
 * appropriate permissions can add, update or delete user annex entities using
 * the standard entity api.  Note that user annex entities do not have a
 * published/unpublished status.
 * *
 * @ingroup user_annex
 *
 * @ContentEntityType(
 *   id = "user_annex",
 *   label = @Translation("User annex"),
 *   handlers = {
 *     "view_builder" = "Drupal\user_annex\UserAnnexViewBuilder",
 *     "list_builder" = "Drupal\user_annex\UserAnnexListBuilder",
 *     "views_data" = "Drupal\user_annex\UserAnnexViewsData",
 *
 *     "form" = {
 *       "add" = "Drupal\user_annex\Form\UserAnnexAddForm",
 *       "edit" = "Drupal\user_annex\Form\UserAnnexEditForm",
 *       "delete" = "Drupal\user_annex\Form\UserAnnexDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\user_annex\UserAnnexHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\user_annex\UserAnnexAccessControlHandler",
 *   },
 *   base_table = "user_annex",
 *   translatable = FALSE,
 *   admin_permission = "administer user_annex entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/user_annex/{user_annex}",
 *     "add-form" = "/user_annex/add",
 *     "edit-form" = "/user_annex/{user_annex}/edit",
 *     "delete-form" = "/user_annex/{user_annex}/delete",
 *     "collection" = "/user_annex/list",
 *   },
 *   field_ui_base_route = "user_annex.settings"
 * )
 */
class UserAnnex extends ContentEntityBase implements UserAnnexInterface {

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $values += array(
      'created' => time(),
      'modified_by' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * Sets the label of the user_annex entity.
   *
   * @param string $label
   *   The label for the user_annex entity.
   *
   * @return \Drupal\user_annex\Entity\UserAnnexInterface
   *   The called user_annex entity.
   */
  public function setLabel(string $label): UserAnnexInterface {
    $this->set('label', $label);
    return $this;
  }

  /**
   * Gets the action used to modify the user entity.
   *
   * @return string
   *   Action used to modify the user entity.
   */
  public function getAction(): string {
    return $this->get('action')->value;
  }

  /**
   * Sets the action used to modify the user entity.
   *
   * @param string $action
   *   The action used to modify the user entity.
   *
   * @return \Drupal\user_annex\Entity\UserAnnexInterface
   *   The called user_annex entity.
   */
  public function setAction(string $action): UserAnnexInterface {
    $this->set('action', $action);
    return $this;
  }

  /**
   * Gets the User id from the modified user entity.
   *
   * @return int
   *   user id from the modified user entity.
   */
  public function getUserId(): int {
    return $this->get('user_id')->value;
  }

  /**
   * Sets the user id from the modified user entity.
   *
   * @param string $uid
   *   The User id from the modified user entity.
   *
   * @return \Drupal\user_annex\Entity\UserAnnexInterface
   *   The called user_annex entity.
   */
  public function setUserid($uid): UserAnnexInterface {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * Gets the creation time of the user_annex entity.
   *
   * @return integer
   *   Creation timestamp of the user_annex entity.
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * Sets the creation time of the user_annex entity.
   *
   * @param integer $timestamp
   *   The creation timestamp for the user_annex entity.
   *
   * @return \Drupal\user_annex\Entity\UserAnnexInterface
   *   The called user_annex entity.
   */
  public function setCreatedTime(int $timestamp): UserAnnexInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * Gets the User account responsible for modifying the user entity.
   *
   * @return integer
   *   Id of user account responsible for user entity change.
   */
  public function getModifiedByUid(): int {
    return $this->get('modified_by')->target_id;
  }

  /**
   * Gets the User account responsible for modifying the user entity.
   *
   * @return \Drupal\user\UserInterface
   *   User account responsible for user entity change.
   */
  public function getModifiedBy(): UserInterface {
    return User::load($this->getModifiedByUid());
  }

  /**
   * Sets the user account responsible for modifying the user entity.
   *
   * @param \Drupal\user\UserInterface $user
   *   The account responsible for modifying the user entity.
   *
   * @return \Drupal\user_annex\Entity\UserAnnexInterface
   *   The called user_annex entity.
   */
  public function setModifiedBy(UserInterface $user): UserAnnexInterface {
    $this->set('modified_by', $user->id());
    return $this;
  }

  /**
   * Sets the user account responsible for modifying the user entity.
   *
   * @param integer $uid
   *   The id of the account responsible for modifying the user entity.
   *
   * @return \Drupal\user_annex\Entity\UserAnnexInterface
   *   The called user_annex entity.
   */
  public function setModifiedByUid(int $uid): UserAnnexInterface {
    $this->set('modified_by', $uid);
    return $this;
  }

  /**
   * Gets the difference between the tracked field values of the modified user
   * entity and a previous copy.
   *
   * @return string
   *   the difference between the modified user entity and a previous copy.
   */
  public function getDifference(): string {
    return $this->get('difference')->value;
  }

  /**
   * Sets the difference between the tracked field values of the modified user
   * entity and a previous copy.
   *
   * @param string $difference
   *   the differences between the modified user entity and a previous copy.
   *
   * @return \Drupal\user_annex\Entity\UserAnnexInterface
   *   The called user_annex entity.
   */
  public function setDifference(string $difference): UserAnnexInterface {
    if (strlen($difference) > 255) {
      $difference = substr($difference,0,252) . '...';
    }
    $this->set('difference', $difference);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    // Start with the base fields for a content entity type.
    // This includes only id for user_annex entities as specified
    // in the annotation above.
    $fields = parent::baseFieldDefinitions($entity_type);

    $weight = 0;

    // Define some fields specific to this entity type.
    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setDescription(t('Auto-create label for the user_annex entity'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => $weight,
    ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['action'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Action'))
      ->setDescription(t('The action used to modify the user entity.'))
      ->setDefaultValue('')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Account ID'))
      ->setDescription(t('The ID of the modified account.'))
      ->setSetting('target_type', 'user')
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_entity_id',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ]
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Modified on'))
      ->setDescription(t('The time that the user_annex entity was created.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['modified_by'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Modified by'))
      ->setDescription(t('The account making the changes to the user entity.'))
      ->setSetting('target_type', 'user')
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => $weight,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ]
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['difference'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Difference'))
      ->setDescription(t('The difference between this user_annex record and the previous one for same user account'))
      ->setDefaultValue('')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  // Dummy methods on missing fields
  public function isPublished(): bool {
    return TRUE;
  }

  public function bundle(): string {
    return 'user_annex';
  }

}
