<?php

namespace Drupal\user_annex\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\UserInterface;
use EasyRdf\Literal\Integer;

/**
 * Provides an interface for defining user_annex entities.
 *
 * @ingroup user_annex
 */
interface UserAnnexInterface extends ContentEntityInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Sets the label of the user_annex entity.
   *
   * @param string $label
   *   The label for the user_annex entity.
   *
   * @return \Drupal\user_annex\Entity\UserAnnexInterface
   *   The called user_annex entity.
   */
  public function setLabel(string $label): UserAnnexInterface;

  /**
   * Gets the action used to modify the user entity.
   *
   * @return string
   *   Action used to modify the user entity.
   */
  public function getAction(): string;

  /**
   * Sets the action used to modify the user entity.
   *
   * @param string $action
   *   The aaction used to modify the user entity.
   *
   * @return \Drupal\user_annex\Entity\UserAnnexInterface
   *   The called user_annex entity.
   */
  public function setAction(string $action): UserAnnexInterface;

  /**
   * Gets the User id from the modified user entity.
   *
   * @return int
   *   user id from the modified user entity.
   */
  public function getUserId(): int;

  /**
   * Sets the user id from the modified user entity.
   *
   * @param int $uid
   *   The User id from the modified user entity.
   *
   * @return \Drupal\user_annex\Entity\UserAnnexInterface
   *   The called user_annex entity.
   */
  public function setUserid(int $uid): UserAnnexInterface;

  /**
   * Gets the User annex creation timestamp.
   *
   * @return int
   *   Creation timestamp of the User annex.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the User annex creation timestamp.
   *
   * @param int $timestamp
   *   The User annex creation timestamp.
   *
   * @return \Drupal\user_annex\Entity\UserAnnexInterface
   *   The called user_annex entity.
   */
  public function setCreatedTime(int $timestamp): UserAnnexInterface;

  /**
   * Gets the User account responsible for modifying the user entity.
   *
   * @return integer
   *   Id of user account responsible for user entity changes.
   */
  public function getModifiedByUid(): int;

  /**
   * Gets the User account responsible for modifying the user entity.
   *
   * @return \Drupal\user\UserInterface
   *   User account responsible for user entity changes.
   */
  public function getModifiedBy(): UserInterface;

  /**
   * Sets the user account responsible for modifying the user entity.
   *
   * @param \Drupal\user\UserInterface $user
   *   The account responsible for modifying the user entity.
   *
   * @return \Drupal\user_annex\Entity\UserAnnexInterface
   *   The called user_annex entity.
   */
  public function setModifiedBy(UserInterface $user): UserAnnexInterface;

  /**
   * Sets the user account responsible for modifying the user entity.
   *
   * @param int $uid
   *   The id of the account responsible for modifying the user entity.
   *
   * @return \Drupal\user_annex\Entity\UserAnnexInterface
   *   The called user_annex entity.
   */
  public function setModifiedByUid(int $uid): UserAnnexInterface;

  /**
   * Gets the difference between the modified user entity and a previous copy.
   *
   * @return string
   *   the difference between the modified user entity and a previous copy.
   */
  public function getDifference(): string;

  /**
   * Sets the user preferred admin langcode from the modified user entity.
   *
   * @param string $difference
   *   the differences between the modified user entity and a previous copy.
   *
   * @return \Drupal\user_annex\Entity\UserAnnexInterface
   *   The called user_annex entity.
   */
  public function setDifference(string $difference): UserAnnexInterface;

}
