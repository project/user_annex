<?php

namespace Drupal\user_annex;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for user_annex entities.
 */
class UserAnnexViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   * @noinspection PhpUnnecessaryLocalVariableInspection
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins,
    // can be put here.

    return $data;
  }

}
