## Description

This module will provide functionlity to track and report on changes to user entities.
The user entity changes are recorded in the database as a user_annex entity that records
the values of the user entity properties after any change (insert or update).
If a user entity is deleted, then an annex record is saved recording the deletion and the
final values of the user entity properties.

The initial release will only record changes to the default properties of a user entity.
These are:
1. 'User name' => 'name',
2. 'User mail' => 'mail',
3. 'User status' => 'status',

The user annex entities can be processed using the standard entity API functions,
except that there is no ability to manually add a new record, update an existing record
or delete one from the table.

Initial values for each existing user account are generated via a batch process after the module
has been installed.  Running this batch process is currently a manual task, but this may be chenged
to be automatic at some point in the future.

The user_annex entities can be viewed using different view modes.  Two are provided by default
- a default mode which displays the full record
- a tab mode for display of key information from each change on a new tab of the user account page.

The user_annex entities can be manually created, updated, or deleted using standard Drupal entity API
functionality.

The user_annex entities are fully compatible with views.  There are two default views provided
with the module:
- a list of all user annex records that can be filtered for records afeecting a specified user account,
  for changes within a date range, for changes involving a specified role, and for changes made by
  a specified user.
- a list of changes (rendered using the tab view mode) for a specified user account to be displayed
  on a new tab on the user account page.

User_annex entities are rendered using the user_annex twig template provided with the module, with a
small CSS file attached to ensure a consistent basic layout.  The template and attached CSS file
can be overridden by adding to a custom theme and modifying if required.

The module will keep any existing records for a user account which is deleted, and will add a new
record to record the deletion to ensure the annex is retained.

## Usage
1. Install the module in the usual way.
2. Run the batch initialise process to create initial values for all existing user accounts.
   Go to "/user_annex/initialise" or visit the status report and click on the link.


James Scott (jlscott)
26 Jul 2022
