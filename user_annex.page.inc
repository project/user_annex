<?php

/**
 * @file
 * Contains user_annex.page.inc.
 *
 * Page callback for user_annex entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for User annex templates.
 *
 * Default template: user_annex.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 *
 * @throws \Drupal\Core\Entity\EntityMalformedException
 */
function template_preprocess_user_annex(array &$variables): void {

  // Fetch UserAnnex Entity Object.
  /** @var \Drupal\user_annex\Entity\UserAnnexInterface $entity */
  $entity = $variables['elements']['#user_annex'];
  //provide the label
  $variables['label'] = $entity->label();
  //provide the alias
  $variables['url'] = $entity->toUrl()->toString();

  // Helpful $content variable for templates.
  $variables['content'] = [];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

}
