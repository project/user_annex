<?php

/**
 * @file
 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
 */

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\user_annex\Entity\UserAnnex;
use Drupal\Core\Url;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Function to archive user_annex records. This function is executed in a batch
 * context.
 *
 * @param string $archive_date
 * @param integer $min_records
 * @param integer $max_records
 * @param string $file_format
 * @param integer $batch_size
 * @param array $context
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function user_annex_archive_records(string $archive_date, int $min_records, int $max_records, string $file_format, int $batch_size, array &$context): void {

  $config = \Drupal::config('user_annex.settings');

  // Convert the $archive_date string to a timestamp
  if (!empty($archive_date)) {
    $archive_date = DrupalDateTime::createFromFormat('Y-m-d', $archive_date);
    $archive_timestamp = $archive_date->format('U');
  }
  else {
    $archive_timestamp = 0;
  }

  // Initialise the sandbox values on the first call.
  // The batch process will iterate through all user ids.
  if (empty($context['sandbox'])) {
    $context['sandbox'] = [];
    $context['sandbox']['total'] = \Drupal::entityQuery('user')
      ->accessCheck(TRUE)
      ->condition('uid', 0, '>')
      ->count()
      ->execute();

    /** @var \Drupal\Core\StreamWrapper\StreamWrapperManager $streamWrapperManager */
    $streamWrapperManager = \Drupal::service('stream_wrapper_manager');
    // Check if the private file stream wrapper is available for use.
    $filepath = $streamWrapperManager->isValidScheme('private') ? 'private://' : 'public://';
    // Append the specified archive file path to the file scheme.
    $filepath .= $config->get('archive.directory') ?? 'user_annex/archive/';
    // Get the file name and append the current date/time.
    $filename = $config->get('archive.filename') ?? 'user-annex-archive';
    $filename .= '-' . date('Ymd') . '-' . date('Hi') . '.' . $file_format;

    // Ensure the filepath exists
    if (!\Drupal::service('file_system')->prepareDirectory($filepath, FileSystemInterface::CREATE_DIRECTORY)) {
      $context['message'] = t('ERROR - Unable to access or create the specified archive directory');
      $context['results']['total'] = 0;
      $context['results']['archived'] = 0;
      $context['finished'] = 1;
      return;
    }

    $context['sandbox']['last'] = 0;
    $context['sandbox']['count'] = 0;
    $context['sandbox']['archived'] = 0;
    $context['sandbox']['ids'] = [];
    $context['sandbox']['filepath'] = $filepath;
    $context['sandbox']['filename'] = $filename;
    $context['sandbox']['header_write'] = FALSE;

    $header_write = FALSE;

    // Initialise the output file.
    $archive_file = fopen($filepath . $filename, "w");
  }
  else {
    // Recover the values of variables from the context sandbox.
    $filepath = $context['sandbox']['filepath'];
    $filename = $context['sandbox']['filename'];
    $header_write = $context['sandbox']['header_write'];

    // Re-open the output file for subsequent batches.
    $archive_file = fopen($filepath . $filename, "a");
  }

  // Retrieve the next set of user ids
  $uids = \Drupal::entityQuery('user')
    ->accessCheck(TRUE)
    ->condition('uid', $context['sandbox']['last'], '>')
    ->sort('uid')
    ->range(0, $batch_size)
    ->execute();

  foreach ($uids as $uid) {

    // Count the user_annex records for this account.
    $num_records = \Drupal::entityQuery('user_annex')
      ->accessCheck(TRUE)
      ->condition('user_id', $uid)
      ->count()
      ->execute();

    // Ensure that there are at least $min_records annex records for this
    // account before considering any archiving.
    if ($num_records > $min_records) {
      $uaids = [];
      // Find records that are earlier then the archive date
      $ids = \Drupal::entityQuery('user_annex')
        ->accessCheck(TRUE)
        ->condition('user_id', $uid)
        ->condition('created', $archive_timestamp, '<')
        ->sort('created')
        ->execute();
      if (count($ids) > 0) {
        // Check that we are not archiving / deleting too many records.
        $remaining = $num_records - count($ids);
        while ($remaining < $min_records) {
          $keep = array_pop($ids);
          $remaining++;
        }
        $uaids = $ids;
      }
      // Find old records that are in excess of the max retained value
      if ($num_records > $max_records) {
        $ids = \Drupal::entityQuery('user_annex')
          ->accessCheck(TRUE)
          ->condition('user_id', $uid)
          ->sort('created')
          ->range(0, $num_records - $max_records)
          ->execute();
        $uaids = array_unique($uaids + $ids);
      }

      // Now load and archive the selected records
      /** @var \Drupal\user_annex\Entity\userAnnex[] $user_annexes */
      $user_annexes = \Drupal::entityTypeManager()->getStorage('user_annex')->loadMultiple($uaids);

      foreach ($user_annexes as $user_annex) {

        $ua_field_info = user_annex_get_user_annex_fields();

        switch ($file_format) {
          case 'txt':
            $record = user_annex_values_to_txt($user_annex) . PHP_EOL;
            break;

          case 'csv':
            $max_cardinality = $config->get('archive.max_cardinality');
            if (!$header_write) {
              $headers = user_annex_csv_headers($ua_field_info, $max_cardinality);
              fwrite($archive_file, implode(',', $headers) . PHP_EOL);
              $header_write = TRUE;
            }
            $record = user_annex_values_to_csv($user_annex, $ua_field_info, $max_cardinality) . PHP_EOL;
            break;

          case 'xml':
            if (!$header_write) {
              $headers = '<?xml version="1.0"?><user_annexes>';
              fwrite($archive_file, $headers . PHP_EOL);
              $header_write = TRUE;
            }
            $record = user_annex_values_to_xml($user_annex) . PHP_EOL;
            break;

          case 'json':
            $record = user_annex_values_to_json($user_annex) . PHP_EOL;
            break;

          default:
            $record = NULL;
        }
        fwrite($archive_file, $record);
        // Update count of archived user annex records
        $context['sandbox']['archived']++;
        // Save the id of the archived user_annex record.
        $context['sandbox']['ids'][] = $user_annex->id();
      }

      // Have processed all the user annex records for this account.
    }
    // Update count of uids processed.
    $context['sandbox']['count']++;
    $context['sandbox']['last'] = $uid;
  }

  // Finished this batch of uids, so close archive file.
  fclose($archive_file);
  // Save header_write state
  $context['sandbox']['header_write'] = $header_write;

  // Check if all uids have been processed or whether there are more batches to go.
  if ($context['sandbox']['count'] < $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
    if ($context['finished'] > 0.99) {
      $context['finished'] = 0.99;
    }
    $context['message'] = t('Processed %count of %total user accounts.', [
      '%count' => $context['sandbox']['count'],
      '%total' => $context['sandbox']['total'],
    ]);
  }
  else {
    $context['finished'] = 1;

    // Add closing tag to XML file if necessary
    if ($file_format == 'xml') {
      $archive_file = fopen($filepath . $filename, "a");
      fwrite($archive_file, '</user_annexes>');
      fclose($archive_file);
    }

    $context['message'] = t('Finished processing %total user accounts. Archived %num user annex records', [
      '%total' => $context['sandbox']['total'],
      '%num' => $context['sandbox']['archived'],
    ]);
    $context['results']['total'] = $context['sandbox']['total'];
    $context['results']['archived'] = $context['sandbox']['archived'];
    $context['results']['ids'] = $context['sandbox']['ids'];
  }
}

/**
 * Function to archive user_annex records. This function is executed in a batch
 * context.
 *
 * @param string $archive_date
 * @param integer $min_records
 * @param integer $max_records
 * @param integer $batch_size
 * @param array $context
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function user_annex_delete_records(string $archive_date, int $min_records, int $max_records, int $batch_size, array &$context): void {

  // Initialise the sandbox values on the first call.
  // The batch process will process the set of archived user_annex ids.
  if (empty($context['sandbox'])) {
    $context['sandbox'] = [];
    $context['sandbox']['total'] = count($context['results']['ids']);
    $context['sandbox']['count'] = 0;
    $context['sandbox']['deleted'] = 0;
  }

  // Extract the next set of user_annex ids
  $uaids = array_slice($context['results']['ids'], $context['sandbox']['count'], $batch_size);

  // Now load and archive the selected records
  $user_annexes = \Drupal::entityTypeManager()->getStorage('user_annex')->loadMultiple($uaids);

  foreach ($user_annexes as $user_annex) {
    $user_annex->delete();
    // Update count of archived user annex records
    $context['sandbox']['deleted']++;
  }

  // Update count of user_annex ids processed.
  $context['sandbox']['count'] = $context['sandbox']['count'] + count($uaids);

  // Check if all ids have been processed or whether there are more batches to go.
  if ($context['sandbox']['count'] < $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
    if ($context['finished'] > 0.99) {
      $context['finished'] = 0.99;
    }
    $context['message'] = t('Processed %count of %total user_annex records.', [
      '%count' => $context['sandbox']['count'],
      '%total' => $context['sandbox']['total'],
    ]);
  }
  else {
    $context['finished'] = 1;
    $context['message'] = t('Finished processing %total user_annex records. Deleted %num user annex records', [
      '%total' => $context['sandbox']['total'],
      '%num' => $context['sandbox']['deleted'],
    ]);
    $context['results']['total2'] = $context['sandbox']['total'];
    $context['results']['deleted'] = $context['sandbox']['deleted'];
  }
}

/**
 * Function to handle completion of batch process to archive and optionally
 * delete user annex records.  Display a message to user and write to log.
 *
 * @param bool $success
 * @param array $results
 * @param array $operations
 *
 * @return \Symfony\Component\HttpFoundation\RedirectResponse
 */
function user_annex_finished_archive_records(bool $success, array $results, array $operations): RedirectResponse {

  if ($success) {
    // Display a completion message to the user.
    $message = t('Processed %total user accounts and archived %num user annex records',
      ['%total' => $results['total'], '%num' => $results['archived']]);

    \Drupal::messenger()->addMessage($message);
    \Drupal::logger('user_annex')->notice($message);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing @operation with arguments : @args', [
      '@operation' => $error_operation[0],
      '@args' => print_r($error_operation[0], TRUE),
    ]);

    \Drupal::messenger()->addMessage($message);
    \Drupal::logger('user_annex')->error($message);
  }

  // Redirect to the system status report page.
  return new RedirectResponse(Url::fromRoute('system.status')->setAbsolute()->toString());
}


/**
 * Function to return an array of values that can be used as a header record
 * for a CSV archive file.
 *
 * @param array $ua_field_info
 * @param integer $max_cardinality
 *
 * @return array
 */
function user_annex_csv_headers(array $ua_field_info, int $max_cardinality): array {

  $headers = [];

  foreach ($ua_field_info as $field_name => $field_info) {
    $cardinality = $field_info['cardinality'];
    unset($field_info['cardinality']);
    if ($cardinality == -1 || $cardinality > $max_cardinality) {
      $headers[] = $field_name . ':serialized';
    }
    elseif ($cardinality == 1) {
      foreach ($field_info as $property) {
        $headers[] = $field_name . ':' . $property;
      }
    }
    else {
      for ($count = 0; $count < $cardinality; $count++) {
        foreach ($field_info as $property) {
          $headers[] = $field_name . ':' . $count . ':' . $property;
        }
      }
    }
  }
  return $headers;
}

/**
 * Function to convert a user_annex entity to a string for adding to a TXT
 * archive file.  The user_annex entity is first converted to an array and
 * then the array is serialized and massaged to deal with embedded newline
 * characters.
 *
 * @param \Drupal\user_annex\Entity\UserAnnex $user_annex
 *
 * @return string
 */
function user_annex_values_to_txt(UserAnnex $user_annex): string {
  // Convert user_annex entity to an array of values
  $ua_values = $user_annex->toArray();
  // Generate a serialised array, and encode CR/LF chars.
  return str_replace(["\n", "\r"], ['\n', '\r'], serialize($ua_values));
}

/**
 * Function to convert a user_annex entity into a string for adding to a CSV
 * archive file. The user_annex entity is first converted to an array and
 * then the elements of the array are processed to generate CSV columns matching
 * the csv header that has been generated.
 *
 * @param \Drupal\user_annex\Entity\UserAnnex $user_annex
 * @param array $ua_field_info
 * @param integer $max_cardinality
 *
 * @return string
 */
function user_annex_values_to_csv(UserAnnex $user_annex, array $ua_field_info, int $max_cardinality): string {
  // Convert user_annex entity to an array of values
  $ua_values = $user_annex->toArray();

  $return = [];
  foreach ($ua_field_info as $field_name => $field_info) {
    $cardinality = $field_info['cardinality'];
    unset($field_info['cardinality']);
    $field_values = $ua_values[$field_name];
    if ($cardinality == -1 || $cardinality > $max_cardinality) {
      $return[] = serialize($field_values);
    }
    else {
      for ($count = 0; $count < $cardinality; $count++) {
        if (isset($field_values[$count])) {
          foreach ($field_info as $property) {
            $return[] = $field_values[$count][$property];
          }
        }
        else {
          foreach ($field_info as $property) {
            $return[] = NULL;
          }
        }
      }
    }
  }

  // Add quotes to any string values and escape commas and quote marks.
  foreach ($return as $key => $value) {
    if (!is_numeric($value) && !is_null($value)) {
      $return[$key] = '"' . str_replace([',', '"'], ['\,', '\"'], trim($value, '"')) . '"';
    }
  }

  // Encode any CR or LF chars present in the return value.
  return str_replace(["\n", "\r"], ['\n', '\r'], implode(',', $return));

}

/**
 * Function to convert a user_annex entity into a serialized XML data structure
 * using the core normalizations and serialization functions.
 *
 * @param \Drupal\user_annex\Entity\UserAnnex $user_annex
 *
 * @return string|array|null
 */
function user_annex_values_to_xml(UserAnnex $user_annex): string|array|null {

  $record = \Drupal::service('serializer')->serialize($user_annex, 'xml');

  // Strip the xml tag from the front of the record and trim any CR or LF characters
  $record = trim(preg_replace('/^<\?xml(.*)\?>/', '', $record));

  // Encode any CR and LF chars in the record.
  $record = str_replace(["\n", "\r"], ['\n', '\r'], $record);

  // Replace the outer Xml tag with "<user_annex> </user_annex>".
  return preg_replace('#^<(.*?)>(.*)</(.*?)>$#', "<user_annex>$2</user_annex>", $record);

}

/**
 * Function to convert a user_annex entity into a serialized JSON data structure
 * using the core normalizations and serialization functions.
 *
 * @param \Drupal\user_annex\Entity\UserAnnex $user_annex
 *
 * @return mixed
 */
function user_annex_values_to_json(UserAnnex $user_annex): mixed {

  return \Drupal::service('serializer')->serialize($user_annex, 'json');

}

/**
 * Function to get the user_annex fields and return an array specifying the
 * properties and cardinality of each field.
 *
 * @return array
 */
function user_annex_get_user_annex_fields(): array {
  $return = [];
  $entity_field_manager = \Drupal::service('entity_field.manager');
  $field_definitions = $entity_field_manager->getFieldDefinitions('user_annex', 'user_annex');
  foreach ($field_definitions as $field_name => $field_definition) {

    if ($field_definition instanceof BaseFieldDefinition) {
      $return[$field_name][] = $field_definition->getMainPropertyName();
      $return[$field_name]['cardinality'] = $field_definition->getCardinality();
    }
    else {
      // Field added through UI or programatically.
      /** @var \Drupal\field\Entity\FieldConfig $field_config */
      $field_config = $field_definition;
      $field_storage_defn = $field_config->getFieldStorageDefinition();
      foreach (array_keys($field_storage_defn->getColumns()) as $property) {
        $return[$field_name][] = $property;
      }
      $return[$field_name]['cardinality'] = $field_storage_defn->getCardinality();
    }
  }
  return $return;
}
