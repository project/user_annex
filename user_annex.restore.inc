<?php

/**
 * @file
 * @noinspection /**
 * @file
 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
 */

use Drupal\user_annex\Entity\UserAnnex;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Function to restore user_annex records. This function is executed in a batch
 * context.
 *
 * @param string $archive_file
 * @param array $uids
 * @param integer $batch_size
 * @param array $context
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityStorageException
 * @noinspection PhpSwitchCanBeReplacedWithMatchExpressionInspection
 * @noinspection PhpUndefinedVariableInspection
 */
function user_annex_restore_records(string $archive_file, array $uids, int $batch_size, array &$context): void {

  // Set the max allowed line length
  $max_line_length = 2048;

  $config = \Drupal::config('user_annex.settings');

  // Initialise the sandbox values on the first call.
  // The batch process will iterate through all user ids.
  if (empty($context['sandbox'])) {

    // Ensure the specified file exists and is readable.
    if (!is_readable($archive_file)) {
      $context['message'] = t('ERROR - The specified archive file either does not exist or is not readable');
      $context['results']['total'] = 0;
      $context['results']['restored'] = 0;
      $context['finished'] = 1;
      return;
    }

    $context['sandbox'] = [];
    $context['sandbox']['total'] = user_annex_count_archive_records($archive_file);
    $context['sandbox']['last'] = 0;
    $context['sandbox']['count'] = 0;
    $context['sandbox']['restored'] = 0;
    $context['sandbox']['offset'] = 0;
  }

  // Determine the archive file format.
  $format = strtolower(substr($archive_file, strrpos($archive_file, '.') + 1));

  // Open the output file for READING.
  $archive_data = fopen($archive_file, "r");

  // Retrieve the CSV header record if necessary.
  if ($format == 'csv') {
    $header_record = trim(fgets($archive_data));

    // Check if record returned
    if (!$header_record) {
      $context['message'] = t('ERROR - The specified archive file is empty!');
      $context['results']['total'] = 0;
      $context['results']['restored'] = 0;
      $context['finished'] = 1;
      return;
    }

    // Convert header record to an array of header values.
    $header = user_annex_csv_to_header($header_record);
    if (empty($header)) {
      $context['message'] = t('ERROR - Unable to extract a header record from the specified archive file!');
      $context['results']['total'] = 0;
      $context['results']['restored'] = 0;
      $context['finished'] = 1;
      return;
    }
  }

  // Position the file pointer to the next data record
  if ($context['sandbox']['offset'] > 0) {
    $result = fseek($archive_data, $context['sandbox']['offset']);
    if ($result < 0) {
      $context['message'] = t('ERROR - File seek to offset %offset failed!', ['%offset' => $context['sandbox']['offset']]);
      $context['results']['total'] = $context['sandbox']['total'];
      $context['results']['restored'] = $context['sandbox']['restored'];
      $context['finished'] = 1;
      return;
    }
  }

  $count = 0;

  // Read the records from the CSV file and process up to $batch_size.
  while (!feof($archive_data) && $count < $batch_size) {

    // Read next record and strip the newline char
    $record = trim(fgets($archive_data));

    // Increment the count of records read from the archive file.
    $context['sandbox']['count']++;

    switch ($format) {
      case 'txt':
        $values = user_annex_txt_to_values($record);
        break;

      case 'csv':
        $values = user_annex_csv_to_values($record, $header);
        break;

      case 'xml':
        $values = user_annex_xml_to_values($record);
        break;

      case 'json':
        $values = user_annex_json_to_values($record);
        break;

      default:
        $values = NULL;
    }

    if ($values && isset($values['user_id'])) {
      // Ignore this record if it is not for the specified users.
      $user_id = $values['user_id'][0]['target_id'];
      if (!empty($uids) && !in_array($user_id, $uids)) {
        continue;
      }

      // Check that this record is not already present in the database
      $record_label = $values['label'][0]['value'];
      $result = \Drupal::entityQuery('user_annex')
        ->accessCheck(TRUE)
        ->condition('label', $record_label)
        ->execute();
      // Ignore if the record is already in the database.
      if ($result) {
        continue;
      }

      // Process this record
      $count++;

      // Add any missing base fields
      foreach (user_annex_get_base_field_values() as $field_name => $field_info) {
        if (empty($values[$field_name])) {
          $values[$field_name] = $field_info;
        }
      }

      // Create a new user annex record.
      $user_annex = new UserAnnex([], 'user_annex');
      // Generate a unique UUID value for this record.
      $user_annex->uuid->value = \Drupal::service('uuid')->generate();

      // Set the values in the user annex record.
      $user_annex->setLabel($values['label'][0]['value']);
      $user_annex->setCreatedTime($values['created'][0]['value']);
      $user_annex->setAction($values['action'][0]['value']);
      $user_annex->setModifiedByUid($values['modified_by'][0]['target_id']);
      $user_annex->setUserId($values['user_id'][0]['target_id']);
      $user_annex->setDifference($values['difference'][0]['value']);

      // Get a list of fields attached to the user_annex entity.
      $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('user_annex', 'user_annex');
      // Get a list of currently tracked fields.
      $tracked_fields = array_merge($config->get('base_fields'), $config->get('attached_fields'));
      if (!empty($tracked_fields)) {
        // Add values of tracked fields attached to the user entity.
        foreach ($tracked_fields as $field_name => $tracked) {
          if ($tracked && isset($values[$field_name])) {
            $field_value = $values[$field_name];
            // Eliminate any empty values.
            foreach ($field_value as $delta => $field_item) {
              foreach ($field_item as $property => $value) {
                if (empty($value)) {
                  unset($field_item[$property]);
                }
              }
              if (empty($field_item)) {
                unset($field_value[$delta]);
              }
            }
            // If there is a real value, then add to the user_annex record.
            if (!empty($field_value)) {
              $user_annex->set($field_name, $field_value);
            }
          }
        }
      }

      // Save the re-created user annex record into the database.
      $user_annex->save();
      // Update count of restored user annex records
      $context['sandbox']['restored']++;

    }
  }

  // Save the file pointer as an offset into the data stream.
  $context['sandbox']['offset'] = ftell($archive_data);

  // Finished this batch of archive records, so close archive file.
  fclose($archive_data);

  // Check if all records have been processed or whether there are more batches to go.
  if ($context['sandbox']['count'] < $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
    if ($context['finished'] > 0.99) {
      $context['finished'] = 0.99;
    }
    $context['message'] = t('Processed %count of %total user accounts.', [
      '%count' => $context['sandbox']['count'],
      '%total' => $context['sandbox']['total'],
    ]);
  }
  else {
    $context['finished'] = 1;
    $context['message'] = t('Finished processing %total user accounts. Restored %num user annex records', [
      '%total' => $context['sandbox']['total'],
      '%num' => $context['sandbox']['restored'],
    ]);
    $context['results']['total'] = $context['sandbox']['total'];
    $context['results']['restored'] = $context['sandbox']['restored'];
  }
}

/**
 * Function to handle completion of batch process to restore user annex
 * records.  Display a message to user and write to log.
 *
 * @param bool $success
 * @param array $results
 * @param array $operations
 *
 * @return \Symfony\Component\HttpFoundation\RedirectResponse
 */
function user_annex_finished_restore_records(bool $success, array $results, array $operations): RedirectResponse {

  if ($success) {
    // Display a completion message to the user.
    $message = t('Processed %total, and restored %restored user annex records from archive file.',
      ['%total' => $results['total'], '%restored' => $results['restored']]);
    \Drupal::messenger()->addMessage($message);
    \Drupal::logger('user_annex')->notice($message);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing @operation with arguments : @args', [
      '@operation' => $error_operation[0],
      '@args' => print_r($error_operation[0], TRUE),
    ]);
    \Drupal::messenger()->addMessage($message);
    \Drupal::logger('user_annex')->error($message);
  }

  // Redirect to the system status report page.
  return new RedirectResponse(Url::fromRoute('system.status')->setAbsolute()->toString());
}

/**
 * Function to count the archive records in a CSV or XML file.
 *
 * @param string $archive_file
 *
 * @return int
 */
function user_annex_count_archive_records(string $archive_file): int {

  $count = 0;
  // Open the output file for READING.
  $archive_data = fopen($archive_file, "r");
  if ($archive_data) {
    while (!feof($archive_data)) {
      $record = fgets($archive_data);
      if($record) {
        $count++;
      }
    }
  }
  fclose($archive_data);
  $format = strtolower(substr($archive_file, strrpos($archive_file, '.') + 1));
  if ($format == 'csv') {
    $count--;
  }
  elseif ($format == 'xml') {
    $count--;
    $count--;
  }

  return $count;

}

/**
 * Function to get the default values for the base fields for a user_annex record.
 *
 * @return array
 */
function user_annex_get_base_field_values(): array {

  return [
    'id' => [['value' => 0]],
    'label' => [['value' => '']],
    'created' => [['value' => '']],
    'action' => [['value' => '']],
    'modified_by' => [['target_id' => 0]],
    'user_deleted' => [['value' => '']],
    'user_id' => [['target_id' => 0]],
    'user_name' => [['value' => '']],
    'user_pass' => [['value' => '']],
    'user_mail' => [['value' => '']],
    'user_timezone' => [['value' => '']],
    'user_status' => [['value' => '']],
    'user_roles' => [['value' => '']],
    'user_created' => [['value' => '']],
    'user_changed' => [['value' => '']],
    'user_access' => [['value' => '']],
    'user_login' => [['value' => '']],
    'user_init' => [['value' => '']],
    'user_langcode' => [['value' => '']],
    'user_preferred_langcode' => [['value' => '']],
    'user_preferred_admin_langcode' => [['value' => '']],
    'difference' => [['value' => '']],
  ];
}

/**
 * Function to decode and unseriaize a text record to an array of values.
 *
 * @param string $record
 *
 * @return mixed
 */
function user_annex_txt_to_values(string $record): mixed {

  // Decode the CR and LF chars
  $record = str_replace(['\r', '\n'], ["\r", "\n"], $record);

  // Unserialize the string
  return unserialize($record);

}

/**
 * Convert a CSV header record to an array of labels.  The return array will
 * have an array for each CSV field, with the field name as the key to either
 * the field property, or an array of the field value instance delta mapped to
 * the field property.
 *
 * @param string $header_record
 *
 * @return array
 */
function user_annex_csv_to_header(string $header_record): array {

  $labels = [];

  // Process the header csv record
  foreach (explode(',', $header_record) as $label) {

    $components = explode(':', $label);

    if (count($components) == 3) {
      [$fieldname, $delta, $property] = $components;
      // Record this header label in the array.
      $labels[] = [$fieldname . ':' . $delta => $property];
    }
    elseif (count($components) == 2) {
      [$fieldname, $property] = $components;
      // Record this header label in the array.
      $labels[] = [$fieldname => $property];
    }
    else {
      [$fieldname] = $components;
      // Record this header label in the array.
      $labels[] = [$fieldname => 'unknown'];
    }
  }

  return $labels;

}

/**
 * Convert a CSV record to an associative array of values.  The array will have
 * the field label as the key mapped to a property value.
 *
 * @param string $record
 * @param array $header
 *
 * @return array
 */
function user_annex_csv_to_values(string $record, array $header = []): array {

  // Convert the CSV record to an array of data values after first encoding
  // real commas.
  $record = str_replace('\,', '&comma;', $record);
  $record_values = explode(',', $record);

  // Check that the array of values is the same size as the header array.
  if (count($record_values) != count($header)) {
    return [];
  }

  $values = [];
  foreach ($header as $index => $field_data) {
    $field_name = key($field_data);
    $property = current($field_data);

    $components = explode(':', $field_name);
    if (count($components) == 2) {
      [$field_name, $delta] = $components;
    }
    else {
      $delta = 0;
    }

    // Get the matching data value.
    $value = $record_values[$index];

    // Process text string value.
    if (!is_numeric($value) && !empty($value)) {
      $value = str_replace(['\n', '\r'], ["\n", "\r"], $value);
      $value = str_replace(['&comma;', '\"'], [',', '"'], $value);
      $value = trim($value, '"');
    }

    // Analyse the header detail for this value and build a data structure.
    if ($property == 'serialized') {
      $values[$field_name] = unserialize($value);
    }
    else {
      // This is a normal field property value
      $values[$field_name][$delta][$property] = $value;
    }

  }

  return $values;

}

/**
 * Convert an XML record to an associative array of values.  The array will have
 * the field label as the key mapped to a property value.
 *
 * @param string $record
 *
 * @return array
 */
function user_annex_xml_to_values(string $record): array {

  // Ignore opening or closing XML tags
  if (strpos($record, 'user_annexes>') > 0) {
    return [];
  }

  // Add XML tag back to front of record
  $record = '<?xml version="1.0"?>' . $record;

  // Xml decode will return an array of values that require alteration to
  // generate the desired item list format
  $array = Drupal::service('serializer')->decode($record, 'xml');

  $values = [];

  foreach ($array as $field_name => $field_items) {
    if (is_array($field_items)) {
      foreach ($field_items as $key => $field_item) {
        if (is_array($field_item)) {
          $values[$field_name][$key] = $field_item;
        }
        else {
          $values[$field_name][0][$key] = $field_item;
        }
      }
    }
  }

  return $values;

}

/**
 * Convert a JSON record to an associative array of values.  The array will have
 * the field label as the key mapped to a property value.
 *
 * @param string $record
 *
 * @return mixed
 */
function user_annex_json_to_values(string $record): mixed {

  // Json decode will return an array of values in correct format.
  return \Drupal::service('serializer')->decode($record, 'json');

}
